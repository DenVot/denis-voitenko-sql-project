-- Получает транзакции со следующими доп данными: количество успешно завершенных транзакций к моменту текущей
SELECT *, count(*) OVER (ORDER BY created_at ) FROM project.transaction WHERE status = 'OK';

-- Получает количество транзакций по статусам
WITH grouped AS (SELECT status, count(*) FROM project.transaction GROUP BY status)
SELECT *, dense_rank() OVER () FROM grouped;

-- Получает для каждого пользователя количество ролей, которые есть у него
SELECT *, count(*) OVER (PARTITION BY role) FROM project.user;

-- Получает средную стоимость всех транзакций, которые были созданы на момент создания текущей
SELECT *, avg(amount) OVER(PARTITION BY created_at) FROM project.transaction;

-- Дает оценку стоимости транзакции
SELECT *, CASE WHEN cost > avg(cost) OVER() THEN 'Больше среднего' ELSE 'Меньше среднего' END
FROM project.item;
