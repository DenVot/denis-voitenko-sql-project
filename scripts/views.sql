-- Подсчитывает сколько людей соответствует какой-либо роли
CREATE VIEW roles_count AS
SELECT role, count(*) FROM project."user"
GROUP BY role;

-- Получает информацию о потраченных деньгах на каждый кошелек
CREATE VIEW money_per_wallet AS
SELECT wallet_id, SUM(amount) FROM project."wallet"
    JOIN project.transaction t on wallet.id = t.wallet_id
GROUP BY wallet_id;

-- Получает все дорогие предметы
CREATE VIEW expensive_items AS
    SELECT * FROM project.item
WHERE cost > 100;

-- Сводная информация по рентам
CREATE VIEW rental_history AS
SELECT r.id AS rent_id, r."from", r."to", i.id AS item_id, i.cost, t.id AS transaction_id, t.status, u.id AS issuer_id
FROM project.rent r
JOIN project.item i ON r.item_id = i.id
LEFT JOIN project.transaction t ON r.transaction_id = t.id
JOIN project."user" u ON r.issuer = u.id;
