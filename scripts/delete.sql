-- Коммит денежной транзакции
UPDATE project.transaction SET status = 'OK' WHERE id = 1;

-- Назначение администратора
UPDATE project.user SET role = 'admin' WHERE id = 1;

UPDATE project.user SET first_name = 'admin' WHERE role = 'admin';

-- Удаление вещей стоимостью больше 5
DELETE FROM project.item WHERE cost > 5;

-- Удаление закомиченных транзакций
DELETE FROM project.transaction WHERE status = 'OK';