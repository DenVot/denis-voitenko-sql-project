-- Проверяет доступность ренты
CREATE FUNCTION check_rent_availability(item_id_to_rent BIGINT, start_date TIMESTAMP, end_date TIMESTAMP) RETURNS BOOLEAN
AS $$
DECLARE
    available BOOLEAN;
BEGIN
    SELECT NOT EXISTS(
        SELECT 1
        FROM project.rent
        WHERE item_id = item_id_to_rent
        AND (
            ("from" BETWEEN start_date AND end_date)
            OR ("to" BETWEEN start_date AND end_date)
            OR (start_date BETWEEN "from" AND "to")
            OR (end_date BETWEEN "from" AND "to")
        )
    ) INTO available;

    RETURN available;
END;
$$ LANGUAGE plpgsql;

-- Считает баланс кошелька
CREATE FUNCTION calculate_wallet_balance(wallet_id_to_calc BIGINT) RETURNS NUMERIC
AS $$
DECLARE
    balance NUMERIC := 0;
BEGIN
    SELECT COALESCE(SUM(amount), 0) INTO balance
    FROM project.transaction
    WHERE wallet_id = wallet_id_to_calc;

    RETURN balance;
END;
$$ LANGUAGE plpgsql;

-- Считает активные ренты
CREATE OR REPLACE FUNCTION count_active_rents(user_id BIGINT)
RETURNS INTEGER AS $$
DECLARE
    active_rents_count INTEGER := 0;
BEGIN
    SELECT COUNT(*)
    INTO active_rents_count
    FROM project.rent
    WHERE issuer = count_active_rents.user_id
    AND "from" <= current_timestamp
    AND "to" >= current_timestamp;

    RETURN active_rents_count;
END;
$$ LANGUAGE plpgsql;

-- Считает суммарную стоимость активных рент
CREATE OR REPLACE FUNCTION calculate_total_rent_cost(start_date TIMESTAMP, end_date TIMESTAMP)
RETURNS NUMERIC AS $$
DECLARE
    total_rent_cost NUMERIC := 0;
BEGIN
    SELECT SUM(cost)
    INTO total_rent_cost
    FROM project.item
    WHERE id IN (SELECT item_id FROM project.rent WHERE "from" BETWEEN start_date AND end_date OR "to" BETWEEN start_date AND end_date);

    RETURN total_rent_cost;
END;
$$ LANGUAGE plpgsql;
