-- Получает стоимость всех вещей, которые дешевле 5
SELECT * FROM project.item WHERE cost <= 5;

-- Получает все успешные транзакции
SELECT * FROM project.transaction WHERE status = 'OK';

-- Получает количество транзакций по статусу
SELECT status, count(*) FROM project.transaction GROUP BY status;

-- Получает количество транзакций по статусу, у которых цена более 10
SELECT status, count(*) FROM project.transaction GROUP BY status, amount HAVING amount > 10;

-- Группирует ренты по заказчику
SELECT issuer, count(*) FROM project.rent GROUP BY issuer;

-- Получает все заказы вместе с заказчиками, которые являются обычными пользователями
SELECT * FROM project.rent r JOIN project."user" u ON u.id = r.issuer AND u.role = 'user';

-- Получает полную сводку по ренте, которые были созданы не ранее 1974 года
SELECT * FROM project.rent r
    JOIN project.item t on t.id = r.item_id
WHERE "from" >= '1970-01-01'::timestamp
